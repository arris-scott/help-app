import { State as CoreState } from '../core/state/core.reducer';
import { State as SharedState } from '../shared/state/shared.reducer';
import { State as DataState } from '../data/state';

export interface State {
  core: CoreState;
  data: DataState;
  shared: SharedState;
}
