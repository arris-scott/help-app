import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import { Component } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import * as fromData from '../data/state';

@Component({
  selector: 'app-article-explorer-container',
  template: '<p>Mock Article Explorer Containter</p>'
})
class MockArticleExplorerContainerComponent { }

@Component({
  selector: 'app-article-container',
  template: '<p>Mock Article Containter</p>'
})
class MockArticleContainerComponent { }

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot(fromData.reducer)
      ],
      declarations: [
        HomeComponent,
        MockArticleExplorerContainerComponent,
        MockArticleContainerComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
