import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { Article } from '../data/models/articles';
import * as articleActions from '../data/state/articles/articles.actions';
import * as applicationActions from '../data/state/application/application.actions';
import * as categoryActions from '../data/state/categories/categories.actions';
import * as fromRoot from '../state/app.state';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  articles: Article[];
  selectedArticleId: string;
  selectedArticle$: Observable<Article>;

  componentActive = true;

  constructor(private store: Store<fromRoot.State>) { }

  ngOnInit() {
    this.store.dispatch(new articleActions.Load());
    this.store.dispatch(new categoryActions.Load());
    this.store.dispatch(new applicationActions.LoadAppPreviews());
  }

}
