import { NgModule } from '@angular/core';
import { TreeModule } from 'angular-tree-component';
import { QuillModule } from 'ngx-quill';

import { SharedModule } from '../shared/shared.module';
import { ArticleEditComponent } from './components/article-edit/article-edit.component';
import { ArticleComponent } from './components/article/article.component';
import { ArticleContainerComponent } from './containers/article-container/article-container.component';

@NgModule({
  declarations: [
    ArticleContainerComponent,
    ArticleComponent,
    ArticleEditComponent
  ],
  imports: [
    SharedModule,
    TreeModule,
    QuillModule
  ],
  exports: [
    ArticleContainerComponent
  ]
})
export class ArticleModule { }
