import { Component, Input } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Store, StoreModule } from '@ngrx/store';

import * as fromData from '../../../data/state';
import { ArticleContainerComponent } from './article-container.component';

@Component({
  selector: 'app-article',
  template: '<p>Mock Article Component</p>'
})
class MockArticleComponent {
  @Input() article;
}

@Component({
  selector: 'app-article-edit',
  template: '<p>Mock Article Edit Component</p>'
})
class MockArticleEditComponent {
  @Input() article;
  @Input() categories;
}

describe('ArticlesContainerComponent', () => {
  let component: ArticleContainerComponent;
  let fixture: ComponentFixture<ArticleContainerComponent>;
  let store: Store<fromData.State>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot(fromData.reducer)
      ],
      declarations: [
        ArticleContainerComponent,
        MockArticleComponent,
        MockArticleEditComponent
      ]
    })
    .compileComponents();

    store = TestBed.get(Store);

    spyOn(store, 'dispatch').and.callThrough();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
