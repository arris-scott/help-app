import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { Article } from '../../../data/models/articles';
import { Category } from '../../../data/models/categories';
import * as fromData from '../../../data/state';
import * as articlesActions from '../../../data/state/articles/articles.actions';
import * as fromShared from '../../../shared/state';
import * as fromRoot from '../../../state/app.state';

@Component({
  selector: 'app-article-container',
  templateUrl: './article-container.component.html',
  styleUrls: ['./article-container.component.scss']
})
export class ArticleContainerComponent implements OnInit {

  article$: Observable<Article>;
  categories$: Observable<Category[]>;

  editMode = false;

  constructor(private store: Store<fromRoot.State>) { }

  ngOnInit() {
    this.article$ = this.store.pipe(
      select(fromShared.getSelectedArticleId)).pipe(
        switchMap(id => this.store.pipe(
          select(fromData.getArticleById, {id}))));

    this.categories$ = this.store.pipe(select(fromData.getCategories));
  }

  toggleEditMode() {
    this.editMode = !this.editMode;
  }

  updateArticle(article: Article): void {
    this.store.dispatch(new articlesActions.UpdateArticle(article));
  }
}
