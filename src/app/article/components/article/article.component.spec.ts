import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { articlesData } from '../../../data/mock/articles-data';
import { ArticleComponent } from './article.component';
import { By } from '@angular/platform-browser';

const article = {...articlesData[0]};

describe('ArticleComponent', () => {
  let component: ArticleComponent;
  let fixture: ComponentFixture<ArticleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleComponent);
    component = fixture.componentInstance;
    component.article = article;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit enterEditMode event when edit button is clicked', () => {
    const editArticleSpy = spyOn(component, 'editArticle').and.callThrough();
    const enterEditModeSpy = spyOn(component.enterEditMode, 'emit');

    const button = fixture.debugElement.query(By.css('button'));
    button.triggerEventHandler('click', null);

    expect(editArticleSpy).toHaveBeenCalled();
    expect(enterEditModeSpy).toHaveBeenCalled();
  });
});
