import { Component, EventEmitter, Input, Output } from '@angular/core';

import { Article } from '../../../data/models/articles';


@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent {
  @Input() article: Article;
  @Output() enterEditMode = new EventEmitter<void>();

  editArticle(): void {
    this.enterEditMode.emit();
  }

}
