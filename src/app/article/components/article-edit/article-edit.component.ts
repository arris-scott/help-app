import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { faMinus, faPen, faPlus } from '@fortawesome/free-solid-svg-icons';

import { CategoryModalService } from '../../../category-modal/category-modal.service';
import { Article } from 'src/app/data/models/articles';
import { Category, CategoryUserActions } from 'src/app/data/models/categories';

@Component({
  selector: 'app-article-edit',
  templateUrl: './article-edit.component.html',
  styleUrls: ['./article-edit.component.scss']
})
export class ArticleEditComponent {
  @Input() article: Article;
  @Input() categories: Category[];
  @Output() saveArticle = new EventEmitter<Article>();
  @Output() addCategory = new EventEmitter<Category>();
  @Output() deleteCategory = new EventEmitter<Category>();
  @Output() updateCategory = new EventEmitter<Category>();
  @Output() exitEditMode = new EventEmitter<void>();

  faPlus = faPlus;
  faPen = faPen;
  faMinus = faMinus;

  editMode = false;

  quillOptions;

  constructor(private categoryModalService: CategoryModalService) {
    this.quillOptions = {
      toolbar: [
        ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
        ['blockquote', 'code-block'],

        [{ 'header': 1 }, { 'header': 2 }],               // custom button values
        [{ 'list': 'ordered' }, { 'list': 'bullet' }],
        [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
        [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent
        [{ 'direction': 'rtl' }],                         // text direction

        [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
        [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

        [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
        [{ 'font': [] }],
        [{ 'align': [] }],

        ['clean'],                                         // remove formatting button

        ['link', 'image', 'video']                         // link and image, video
      ]
    };
  }

  submitUpdates(form: NgForm): void {
    this.saveArticle.emit({
      ...this.article,
      ...form.value
    });
    this.stopEditing();
  }

  stopEditing(): void {
    this.exitEditMode.emit();
  }

  openCategoryModal(modalType: CategoryUserActions, categoryId?: string) {
    this.categoryModalService.open(modalType, categoryId);
  }
}
