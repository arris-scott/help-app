import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { articlesData, categoriesData } from '../../../data/mock/articles-data';
import { Article } from '../../../data/models/articles';
import { ArticleEditComponent } from './article-edit.component';
import { QuillModule } from 'ngx-quill';

const article: Article = {...articlesData[0]};
const categories = [...categoriesData];

describe('ArticleEditComponent', () => {
  let component: ArticleEditComponent;
  let fixture: ComponentFixture<ArticleEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        QuillModule
      ],
      declarations: [ ArticleEditComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleEditComponent);
    component = fixture.componentInstance;
    component.categories = categories;
    component.article = article;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
