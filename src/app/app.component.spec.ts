import { HttpClientModule } from '@angular/common/http';
import { async, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { TreeModule } from 'angular-tree-component';

import { AppComponent } from './app.component';
import { ArticleExplorerModule } from './article-explorer/article-explorer.module';
import { ArticleModule } from './article/article.module';
import { CoreModule } from './core/core.module';
import { HomeComponent } from './home/home.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        CoreModule,
        HttpClientModule,
        ArticleModule,
        ArticleExplorerModule,
        StoreModule.forRoot({}),
        EffectsModule.forRoot([]),
        TreeModule.forRoot()
      ],
      declarations: [
        AppComponent,
        HomeComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
