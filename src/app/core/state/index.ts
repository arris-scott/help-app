import { createFeatureSelector, createSelector } from '@ngrx/store';

import { State } from './core.reducer';

const getCoreFeatureState = createFeatureSelector<State>('core');

export const getProductTitle = createSelector(
  getCoreFeatureState,
  state => state.productTitle
);
