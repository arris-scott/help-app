import { Action } from '@ngrx/store';

export enum CoreActionTypes {
  SetProductTitle = '[Core] Set Prodcut Title'
}

export class SetProductTitle implements Action {
  readonly type = CoreActionTypes.SetProductTitle;
  constructor(public payload: string) {}
}

export type CoreActions = SetProductTitle;
