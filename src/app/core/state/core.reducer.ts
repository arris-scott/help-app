import { CoreActions, CoreActionTypes } from './core.actions';

export interface State {
  productTitle: string;
}

const initState: State = {
  productTitle: 'ECO Assist'
};

export function reducer(state: State = initState, action: CoreActions): State {
  switch (action.type) {
    case CoreActionTypes.SetProductTitle:
      return {
        ...state,
        productTitle: action.payload
      };
    default:
      return state;
  }
}
