import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';

import { SharedModule } from '../shared/shared.module';
import { HeaderComponent } from './header/header.component';
import { reducer } from './state/core.reducer';

@NgModule({
  declarations: [HeaderComponent],
  imports: [
    SharedModule,
    StoreModule.forFeature('core', reducer)
  ],
  exports: [
    HeaderComponent
  ]
})
export class CoreModule { }
