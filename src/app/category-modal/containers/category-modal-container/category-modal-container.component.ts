import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { select, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { switchMap, takeWhile } from 'rxjs/operators';

import { Category, CategoryUserActions } from '../../../data/models/categories';
import * as fromData from '../../../data/state';
import * as categoriesActions from '../../../data/state/categories/categories.actions';
import * as fromRoot from '../../../state/app.state';

@Component({
  selector: 'app-category-modal-container',
  templateUrl: './category-modal-container.component.html',
  styleUrls: ['./category-modal-container.component.scss']
})
export class CategoryModalContainerComponent implements OnInit, OnDestroy {
  @Input() modalType: CategoryUserActions;
  @Input() categoryId: string;

  category$: Observable<Category>;

  modalTitle: string;
  categories: Category[];

  componentActive = true;

  constructor(
    public activeModal: NgbActiveModal,
    private store: Store<fromRoot.State>) { }

  ngOnInit() {

    this.modalTitle = this.setModalTitle(this.modalType);

    const categories$ = this.store.pipe(
      select(fromData.getCategories),
      takeWhile(() => this.componentActive));

    categories$.subscribe(cats => this.categories = cats);

    this.category$ = categories$.pipe(
      switchMap(cats => of(cats.find(cat => cat.id === this.categoryId))));
  }

  ngOnDestroy() {
    this.componentActive = false;
  }

  createCategory(category: Category): void {
    this.store.dispatch(new categoriesActions.CreateCategory(category));
    this.close();
  }

  deleteCategory(id: string): void {
    this.store.dispatch(new categoriesActions.DeleteCategory(id));
    this.close();
  }

  updateCategory(category: Category): void {
    this.store.dispatch(new categoriesActions.UpdateCategory(category));
    this.close();
  }

  close(): void {
    this.activeModal.close();
  }

  private setModalTitle(modalType: CategoryUserActions): string {
    switch (this.modalType) {
      case 'create':
        return 'New Category';
      case 'delete':
        return 'Delete Category';
      case 'update':
        return 'Edit Category';
      default:
        break;
    }
  }

}
