import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryModalContainerComponent } from './category-modal-container.component';

xdescribe('CategoryModalContainerComponent', () => {
  let component: CategoryModalContainerComponent;
  let fixture: ComponentFixture<CategoryModalContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryModalContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryModalContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
