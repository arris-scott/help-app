import { Component, EventEmitter, Input, Output } from '@angular/core';

import { Category } from '../../../data/models/categories';

@Component({
  selector: 'app-create-category',
  templateUrl: './create-category.component.html',
  styleUrls: ['./create-category.component.scss']
})
export class CreateCategoryComponent {
  @Input() categories: Category[];
  @Output() createCategory = new EventEmitter<Category>();
  @Output() close = new EventEmitter<void>();

  submit(categoryTitle: string): void {
    this.createCategory.emit(new Category(categoryTitle));
  }
  cancel(): void {
    this.close.emit();
  }
}
