import { Component, EventEmitter, Input, Output } from '@angular/core';

import { Category } from '../../../data/models/categories';

@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.scss']
})
export class EditCategoryComponent {
  @Input() category: Category;
  @Output() updateCategory = new EventEmitter<Category>();
  @Output() close = new EventEmitter<void>();

  submit(title: string): void {
    this.updateCategory.emit({...this.category, title});
  }
  cancel(): void {
    this.close.emit();
  }
}
