import { Component, EventEmitter, Input, Output } from '@angular/core';

import { Category } from '../../../data/models/categories';

@Component({
  selector: 'app-delete-category',
  templateUrl: './delete-category.component.html',
  styleUrls: ['./delete-category.component.scss']
})
export class DeleteCategoryComponent {
  @Input() category: Category;
  @Output() deleteCategory = new EventEmitter<string>();
  @Output() close = new EventEmitter<void>();

  submit(id: string): void {
    this.deleteCategory.emit(id);
  }
  cancel(): void {
    this.close.emit();
  }
}
