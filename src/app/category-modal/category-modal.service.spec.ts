import { TestBed } from '@angular/core/testing';

import { CategoryModalService } from './category-modal.service';

describe('CategoryModalService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CategoryModalService = TestBed.get(CategoryModalService);
    expect(service).toBeTruthy();
  });
});
