import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { CategoryUserActions } from '../data/models/categories';
import { CategoryModalContainerComponent } from './containers/category-modal-container/category-modal-container.component';

@Injectable({
  providedIn: 'root'
})
export class CategoryModalService {

  constructor(private modalService: NgbModal) { }

  open(modalType: CategoryUserActions, categoryId?: string) {
    const modalRef = this.modalService.open(CategoryModalContainerComponent);

    modalRef.componentInstance.modalType = modalType;
    modalRef.componentInstance.categoryId = categoryId;
    // if (categoryId) {
    //   modalRef.componentInstance.category = categories.find(category => category.id === categoryId);
    // }
  }
}
