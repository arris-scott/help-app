import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { CategoryModalContainerComponent } from './containers/category-modal-container/category-modal-container.component';
import { CreateCategoryComponent } from './components/create-category/create-category.component';
import { EditCategoryComponent } from './components/edit-category/edit-category.component';
import { DeleteCategoryComponent } from './components/delete-category/delete-category.component';

@NgModule({
  declarations: [
    CategoryModalContainerComponent,
    CreateCategoryComponent,
    EditCategoryComponent,
    DeleteCategoryComponent
  ],
  imports: [
    SharedModule
  ],
  exports: [CategoryModalContainerComponent]
})
export class CategoryModalModule { }
