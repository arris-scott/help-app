import { IconDefinition } from '@fortawesome/fontawesome-common-types';

import { Article } from '../../data/models/articles';
import { Category } from '../../data/models/categories';

// export type ListView = 'articles' | 'alphabetized' | 'search';
// export type NodeType = 'article' | 'category' | 'searchResult';
export enum ListView {
  articles = 'articles',
  alphabetized = 'alphabetized',
  search = 'search'
}
export enum NodeType {
  article = 'article',
  category = 'category',
  searchResult = 'searchResult'
}

export interface ViewTab {
  view: ListView;
  icon: IconDefinition;
}

export interface TreeNodeData {
  type: NodeType;
  children?: ArticleNode[];
}

export interface SearchNodeData {
  type: NodeType;
  summary: string;
}

export interface ArticleNode extends Article, TreeNodeData { }

export interface CategoryNode extends Category, TreeNodeData { }

export interface SearchNode extends Article, SearchNodeData { }

export type Node = ArticleNode | CategoryNode | SearchNode;

export type TreeNode = ArticleNode | CategoryNode;

export interface ArticleMap { [k: string]: Article; }
