import { Article } from '../../data/models/articles';
import { Category } from '../../data/models/categories';

export type NodeType = 'article' | 'category';

export interface Node {
  data: Article | Category;
  type: NodeType;
  children?: Node[];
  summary?: string;
}

export interface NodeMap {
  [id: string]: Node;
}
