import { TestBed } from '@angular/core/testing';

import { TreeListService } from './tree-list.service';

describe('TreeListService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TreeListService = TestBed.get(TreeListService);
    expect(service).toBeTruthy();
  });
});
