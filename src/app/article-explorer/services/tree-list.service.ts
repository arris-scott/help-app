import { Injectable } from '@angular/core';
import { ITreeModel } from 'angular-tree-component/dist/defs/api';

import { Article } from '../../data/models/articles';
import { Category } from '../../data/models/categories';
import { Utils } from '../../shared/utils.service';
import { ArticleNode, NodeType, TreeNode } from '../models/list';

@Injectable({
  providedIn: 'root'
})
export class TreeListService {
  private articleMap: { [id: string]: Article };

  constructor(private utils: Utils) { }

  buildTreeData(articles: Article[], categories: Category[]): TreeNode[] {
    return categories
      ? this.buildCategoryTreeData(categories, articles)
      : this.buildArticleTreeData(articles);
  }

  async getChangedArticles(tree: ITreeModel): Promise<Article[]> {
    const changedArticles = [];

    await tree.doForAll(node => {
      if (node.data.type === 'category') {
        return;
      }
      const article = this.articleMap[node.data.id];
      const articleData = this.getChangeData(article);
      const nodeData = this.getChangeData(node.data);
      if (!this.utils.equalJson(articleData, nodeData)) {
        changedArticles.push({ ...article, ...nodeData });
      }
    });

    return changedArticles;
  }

  setArticleMap(articles: Article[]) {
    this.articleMap = articles.reduce((articleMap, article) => {
      articleMap[article.id] = article;
      return articleMap;
    }, {});
  }

  private buildArticleTreeData(articles: Article[], root = true): TreeNode[] {
    const levelArticles = root ? articles.filter(article => article.rootArticle) : articles;

    return levelArticles
      .map((article) => {
        const children = article.childrenIds.map(child => this.articleMap[child]);
        return this.buildTreeNode(
          article,
          NodeType.article,
          children.length ? this.buildArticleTreeData(children, false) : []);
      })
      .sort((a: ArticleNode, b: ArticleNode) => a.order - b.order);
  }

  private buildCategoryTreeData(categories: Category[], articles: Article[]): TreeNode[] {
    const categoryNodeMap = categories.reduce((map, category) => {
      map[category.id] = this.buildTreeNode(category, NodeType.category);
      return map;
    }, {});

    const uncategorizedArticles: TreeNode[] = [];

    articles.forEach(article => {
      const parentCategory = categoryNodeMap[article.categoryId];
      const articleNode = this.buildTreeNode(article, NodeType.article);
      parentCategory
        ? parentCategory.children.push(articleNode)
        : uncategorizedArticles.push(articleNode);
    });

    return [...Object.values<TreeNode>(categoryNodeMap), ...uncategorizedArticles];
  }

  private buildTreeNode(data: Article | Category, type: NodeType, children = []): TreeNode {
    return {
      ...data,
      type,
      children
    };
  }

  private getChangeData(data): {} {
    return {
      order: data.order,
      rootArticle: data.rootArticle,
      categoryId: data.categoryId,
      childrenIds: data.childrenIds
    };
  }
}
