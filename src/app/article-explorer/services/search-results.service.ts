import { Injectable } from '@angular/core';

import { Article } from '../../data/models/articles';
import { ArticleNode, NodeType, SearchNode } from '../models/list';

@Injectable({
  providedIn: 'root'
})
export class SearchResultsService {
  nodeType = NodeType;

  constructor() { }

  getSearchNodes(articles: Article[], term: string): ArticleNode[] {
    return term.length
      ? this.getFilteredNodes(articles, term)
      : articles.map(article => this.buildSearchNode(article));
  }

  private getFilteredNodes(articles: Article[], term: string): ArticleNode[] {
    return articles.reduce((nodes, article) => {
      return article.title.toLowerCase().includes(term.toLowerCase())
        ? [...nodes, this.buildSearchNode(article)]
        : nodes;
    }, []);
  }

  private buildSearchNode(article: Article): SearchNode {
    return {...article, type: NodeType.searchResult, summary: ''};
  }
}
