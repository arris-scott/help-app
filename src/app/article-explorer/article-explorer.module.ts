import { NgModule } from '@angular/core';
import { TreeModule } from 'angular-tree-component';

import { SharedModule } from '../shared/shared.module';
import { ListNodeComponent } from './components/list-node/list-node.component';
import { ListTabsComponent } from './components/list-tabs/list-tabs.component';
import { SearchResultsListComponent } from './components/search-results-list/search-results-list.component';
import { TreeListComponent } from './components/tree-list/tree-list.component';
import {
  ArticleExplorerContainerComponent,
} from './containers/article-explorer-container/article-explorer-container.component';
import { ArticleSummaryPipe } from './pipes/article-summary.pipe';

@NgModule({
  declarations: [
    ArticleExplorerContainerComponent,
    ListNodeComponent,
    TreeListComponent,
    ListTabsComponent,
    SearchResultsListComponent,
    ArticleSummaryPipe
  ],
  imports: [
    SharedModule,
    TreeModule
  ],
  exports: [
    ArticleExplorerContainerComponent
  ]
})
export class ArticleExplorerModule { }
