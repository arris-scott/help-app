import { Component, Input } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { articlesData } from 'src/app/data/mock/articles-data';

import { NodeType } from '../../models/list';
import { SearchResultsListComponent } from './search-results-list.component';

@Component({
  selector: 'app-list-node',
  template: '<p>Mock List Node Component</p>'
})
class MockListNodeComponent {
  @Input() node;
  @Input() selectedArticleId;
}

const searchResultNodes = articlesData.map(article => {
  return {
    ...article,
    type: NodeType.searchResult,
    summary: ''
  };
});

describe('SearchResultsListComponent', () => {
  let component: SearchResultsListComponent;
  let fixture: ComponentFixture<SearchResultsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchResultsListComponent, MockListNodeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchResultsListComponent);
    component = fixture.componentInstance;
    component.searchResults = searchResultNodes;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
