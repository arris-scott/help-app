import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { Article } from 'src/app/data/models/articles';

import { SearchResultsService } from '../../services/search-results.service';

@Component({
  selector: 'app-search-results-list',
  templateUrl: './search-results-list.component.html',
  styleUrls: ['./search-results-list.component.scss']
})
export class SearchResultsListComponent implements OnChanges {
  @Input() private articles: Article[];
  @Input() private searchTerm: string;
  @Input() selectedArticleId: string;
  @Output() setSelectedArticleId = new EventEmitter<string>();

  searchResults: Article[];

  constructor(private searchResultsService: SearchResultsService) { }

  // TODO: fix type error
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.articles || changes.searchTerm) {
      this.searchResults = this.searchResultsService.getSearchNodes(this.articles, this.searchTerm);
    }
  }

  selectArticle(id: string): void {
    this.setSelectedArticleId.emit(id);
  }
}
