import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreeListComponent } from './tree-list.component';
import { Component, Input, NO_ERRORS_SCHEMA } from '@angular/core';
import { TreeModule } from 'angular-tree-component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SharedModule } from 'src/app/shared/shared.module';

@Component({
  selector: 'app-list-node',
  template: '<p>Mock List Node Component</p>'
})
class MockListNodeComponent {
  @Input() node;
  @Input() selectedArticleId;
}

describe('TreeListComponent', () => {
  let component: TreeListComponent;
  let fixture: ComponentFixture<TreeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        // TreeModule.forRoot(),
        FontAwesomeModule
      ],
      declarations: [
        TreeListComponent,
        MockListNodeComponent
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
