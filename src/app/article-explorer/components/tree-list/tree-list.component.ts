import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { faFileAlt } from '@fortawesome/free-regular-svg-icons';
import { faListUl, faMinus, faPlus } from '@fortawesome/free-solid-svg-icons';
import { ITreeOptions, TreeComponent } from 'angular-tree-component';
import { ITreeModel, ITreeNode } from 'angular-tree-component/dist/defs/api';

import { Article } from '../../../data/models/articles';
import { Category, CategoryUserActions } from '../../../data/models/categories';
import { Utils } from '../../../shared/utils.service';
import { TreeNode } from '../../models/list';
import { TreeListService } from '../../services/tree-list.service';

@Component({
  selector: 'app-tree-list',
  templateUrl: './tree-list.component.html',
  styleUrls: ['./tree-list.component.scss']
})
export class TreeListComponent implements OnChanges, OnInit {

  @Input() private articles: Article[];
  @Input() categories: Category[];
  @Input() selectedArticleId: string;
  @Output() setSelectedArticleId = new EventEmitter<string>();
  @Output() updateArticles = new EventEmitter<Article[]>();
  @Output() openCategoryModal = new EventEmitter<{ modalType: CategoryUserActions, categoryId: string }>();

  @ViewChild(TreeComponent) private tree: TreeComponent;

  treeNodes: TreeNode[];
  treeOptions: ITreeOptions;

  faPlus = faPlus;
  faMinus = faMinus;
  faFileAlt = faFileAlt;
  faListUl = faListUl;

  private treeResetRef: TreeNode[];

  constructor(
    private utils: Utils,
    private treeListService: TreeListService) { }

  ngOnInit(): void {
    this.treeOptions = {
      displayField: 'title',
      actionMapping: {
        mouse: {
          click: null
        }
      },
      allowDrag: this.allowNodeDrag,
      allowDrop: this.allowNodeDrop.bind(this)
    };
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.articles || changes.categories) {
      if (changes.articles) {
        this.treeListService.setArticleMap(this.articles);
      }
      this.updateTree();
    }
  }

  selectArticle(id: string): void {
    this.setSelectedArticleId.emit(id);
  }

  editCategory($event): void {
    this.openCategoryModal.emit($event);
  }

  updateNodeData($event): void {
    this.categories ? this.updateCategoryData($event) : this.updateArticleData();
  }

  resetTree() {
    this.treeNodes = this.utils.jsonDeepCopy(this.treeResetRef);
    this.tree.treeModel.update();
  }

  async saveTree(tree: ITreeModel) {
    const changedNodes = await this.treeListService.getChangedArticles(tree);
    if (changedNodes.length) {
      this.updateArticles.emit(changedNodes);
    }
  }

  private updateTree() {
    this.treeNodes = this.treeListService.buildTreeData(this.articles, this.categories);
    this.treeResetRef = this.utils.jsonDeepCopy(this.treeNodes);
    this.tree.treeModel.update();
  }

  private updateArticleData(): void {
    this.tree.treeModel.doForAll(node => {
      const articleData = node.data;
      articleData.rootArticle = node.isRoot ? true : false;
      articleData.order = node.index;
      articleData.childrenIds = node.children.map(child => child.id);
    });
  }

  private updateCategoryData($event): void {
    const articleData = $event.node;
    const category = $event.to.parent;
    articleData.categoryId = category.virtual ? '' : category.id;
  }

  private allowNodeDrag(node: ITreeNode): boolean {
    return node.data.type === 'article';
  }

  private allowNodeDrop(node: ITreeNode, parent: ITreeNode): boolean {
    const parentData = parent.parent.data;
    return this.categories ? parentData.type === 'category' || parentData.virtual : true;
  }
}
