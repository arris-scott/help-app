import { Component, EventEmitter, Input, Output } from '@angular/core';
import { faMinus, faPen } from '@fortawesome/free-solid-svg-icons';

import { CategoryUserActions } from '../../../data/models/categories';
import { Node, NodeType } from '../../models/list';

@Component({
  selector: 'app-list-node',
  templateUrl: './list-node.component.html',
  styleUrls: ['./list-node.component.scss']
})
export class ListNodeComponent {
  @Input() node: Node;
  @Input() selectedArticleId: string;
  @Output() setSelectedArticleId = new EventEmitter<string>();
  @Output() openCategoryModal = new EventEmitter<{ modalType: CategoryUserActions, categoryId: string }>();

  faPen = faPen;
  faMinus = faMinus;

  nodeType = NodeType;

  constructor() { }

  selectArticle(id: string): void {
    this.setSelectedArticleId.emit(id);
  }

  editCategory(modalType: CategoryUserActions, categoryId: string) {
    this.openCategoryModal.emit({ modalType, categoryId });
  }
}
