import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { articlesData } from 'src/app/data/mock/articles-data';

import { NodeType, SearchNode, TreeNode } from '../../models/list';
import { ArticleSummaryPipe } from '../../pipes/article-summary.pipe';
import { ListNodeComponent } from './list-node.component';

const articleNode: TreeNode = {
  ...articlesData[0],
  type: NodeType.article,
  children: []
};

const searchResultNode: SearchNode = {
  ...articlesData[0],
  type: NodeType.searchResult,
  summary: ''
};

describe('ListNodeComponent', () => {
  let component: ListNodeComponent;
  let fixture: ComponentFixture<ListNodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ FontAwesomeModule ],
      declarations: [ ListNodeComponent, ArticleSummaryPipe ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListNodeComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    component.node = articleNode;
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should emit selected article ID when article node is clicked', () => {
    component.node = searchResultNode;
    fixture.detectChanges();
    const selectArticleSpy = spyOn(component, 'selectArticle').and.callThrough();
    const selectedArticleIdSpy = spyOn(component.setSelectedArticleId, 'emit');
    const button = fixture.debugElement.query(By.css('button'));
    button.triggerEventHandler('click', null);

    expect(selectArticleSpy).toHaveBeenCalledWith(component.node.id);
    expect(selectedArticleIdSpy).toHaveBeenCalledWith(component.node.id);
  });

  it('should emit selected article ID when search result node is clicked', () => {
    component.node = searchResultNode;
    fixture.detectChanges();
    const selectArticleSpy = spyOn(component, 'selectArticle').and.callThrough();
    const selectedArticleIdSpy = spyOn(component.setSelectedArticleId, 'emit');
    const button = fixture.debugElement.query(By.css('button'));
    button.triggerEventHandler('click', null);

    expect(selectArticleSpy).toHaveBeenCalledWith(component.node.id);
    expect(selectedArticleIdSpy).toHaveBeenCalledWith(component.node.id);
  });
});
