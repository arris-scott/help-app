import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { faListUl, faSearch, faSortAlphaDown } from '@fortawesome/free-solid-svg-icons';

import { ViewTab, ListView } from '../../../article-explorer/models/list';

@Component({
  selector: 'app-list-tabs',
  templateUrl: './list-tabs.component.html',
  styleUrls: ['./list-tabs.component.scss']
})
export class ListTabsComponent implements OnInit {
  tabs: ViewTab[];

  @Input() view: ListView;
  @Output() setListView = new EventEmitter<ListView>();

  listView = ListView;

  constructor() {
    this.tabs = [
      {
        view: this.listView.articles,
        icon: faListUl
      },
      {
        view: this.listView.alphabetized,
        icon: faSortAlphaDown
      },
      {
        view: this.listView.search,
        icon: faSearch
      }
    ];
  }

  ngOnInit() {
  }

  selectListView(view: ListView): void {
    console.log(view);
    this.setListView.emit(view);
  }

}
