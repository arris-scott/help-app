import { Component, Input } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Store, StoreModule } from '@ngrx/store';

import * as fromData from '../../../data/state';
import * as fromShared from '../../../shared/state/shared.reducer';
import { ArticleExplorerContainerComponent } from './article-explorer-container.component';

interface MockState extends fromData.State, fromShared.State { }

@Component({
  selector: 'app-list-tabs',
  template: '<p>Mock List Tabs Component</p>'
})
class MockListTabsComponent {
  @Input() view;
}
@Component({
  selector: 'app-tree-list',
  template: '<p>Mock Tree List Component</p>'
})
class MockTreeListComponent {
  @Input() articles;
  @Input() selectedArticleId;
  @Input() categories;
}
@Component({
  selector: 'app-search',
  template: '<p>Mock Search Component</p>'
})
class MockSearchComponent { }
@Component({
  selector: 'app-search-results-list',
  template: '<p>Mock Search Results List Component</p>'
})
class MockSearchResultsListComponent {
  @Input() articles;
  @Input() searchTerm;
  @Input() selectedArticleId;
}

describe('ArticleExplorerContainerComponent', () => {
  let component: ArticleExplorerContainerComponent;
  let fixture: ComponentFixture<ArticleExplorerContainerComponent>;
  let store: Store<MockState>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({
          ...fromData.reducer,
          ...fromShared.reducer
        })
      ],
      declarations: [
        ArticleExplorerContainerComponent,
        MockListTabsComponent,
        MockTreeListComponent,
        MockSearchComponent,
        MockSearchResultsListComponent
      ]
    })
      .compileComponents();

    store = TestBed.get(Store);

    spyOn(store, 'dispatch').and.callThrough();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleExplorerContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
