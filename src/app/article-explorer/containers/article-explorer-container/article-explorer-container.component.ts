import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { takeWhile } from 'rxjs/operators';

import { CategoryModalService } from '../../../category-modal/category-modal.service';
import { Article } from '../../../data/models/articles';
import { Category, CategoryUserActions } from '../../../data/models/categories';
import * as fromData from '../../../data/state';
import * as articlesActions from '../../../data/state/articles/articles.actions';
import * as categoryActions from '../../../data/state/categories/categories.actions';
import * as fromShared from '../../../shared/state';
import * as sharedActions from '../../../shared/state/shared.actions';
import * as fromRoot from '../../../state/app.state';
import { ListView } from '../../models/list';

@Component({
  selector: 'app-article-explorer-container',
  templateUrl: './article-explorer-container.component.html',
  styleUrls: ['./article-explorer-container.component.scss']
})
export class ArticleExplorerContainerComponent implements OnInit {

  categories$: Observable<Category[]>;
  selectedArticleId$: Observable<string>;
  searchTerm$: Observable<string>;

  articles: Article[];
  searchTerm: string;
  view: ListView = ListView.articles;

  listView = ListView;
  componentActive = true;

  constructor(
    private store: Store<fromRoot.State>,
    private categoryModalService: CategoryModalService) { }

  ngOnInit() {
    this.store.pipe(
      select(fromData.getArticles),
      takeWhile(() => this.componentActive))
      .subscribe(articles => this.articles = articles);

    this.store.pipe(
      select(fromShared.getSearchTerm),
      takeWhile(() => this.componentActive))
      .subscribe(searchTerm => {
        if (this.searchTerm !== undefined) {
          this.setListView(ListView.search);
        }
        this.searchTerm = searchTerm;
      });

      console.log('after');

    this.categories$ = this.store.pipe(select(fromData.getCategories));
    this.selectedArticleId$ = this.store.pipe(select(fromShared.getSelectedArticleId));
    // this.searchTerm$ = this.store.pipe(select(fromShared.getSearchTerm));
  }

  setListView(view: ListView): void {
    this.view = view;
  }

  setSelectedArticleId(id: string): void {
    this.store.dispatch(new sharedActions.SetSelectedArticleId(id));
  }

  deleteCategory(id: string): void {
    const effectedArticles = this.articles.filter(article => article.categoryId === id)
      .map(article => {
        return {
          ...article,
          categoryId: undefined
        };
      });

    this.store.dispatch(new categoryActions.DeleteCategory(id));
    this.updateArticleList(effectedArticles);
  }

  private updateArticleList(articles: Article[]): void {
    articles.forEach(article => this.updateArticle(article));
  }

  private updateArticle(article: Article): void {
    this.store.dispatch(new articlesActions.UpdateArticle(article));
  }

  editCategories($event) {
    this.openCategoryModal($event.modalType, $event.categoryId);
  }

  openCategoryModal(modalType: CategoryUserActions, categoryId?: string) {
    this.categoryModalService.open(modalType, categoryId);
  }
}
