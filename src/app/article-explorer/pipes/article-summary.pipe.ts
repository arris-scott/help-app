import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'articleSummary'
})
export class ArticleSummaryPipe implements PipeTransform {

  openingTag = '<p>';
  closingTag = '</p>';

  transform(value: string): string {
    const startIndex = value.indexOf(this.openingTag) + this.openingTag.length;
    const closingTagIndex = value.indexOf(this.closingTag);
    const firstParagraph = value.substring(startIndex, closingTagIndex);
    const shortened = firstParagraph.substring(0, 100);
    const trimmed = shortened.substring(0, shortened.lastIndexOf(' '));

    const summary = trimmed[trimmed.length - 1] === '.'
      ? trimmed + '..'
      : trimmed + '...';

    return summary;
  }

}
