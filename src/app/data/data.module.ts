import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { reducer } from './state';
import { ArticlesEffects } from './state/articles/articles.effects';
import { CategoriesEffects } from './state/categories/categories.effects';
import { ApplicationEffects } from './state/application/application.effects';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature('data', reducer),
    EffectsModule.forFeature([
      ArticlesEffects,
      CategoriesEffects,
      ApplicationEffects
    ])
  ]
})
export class DataModule { }
