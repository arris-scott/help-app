import { UUID } from 'angular2-uuid';

import { Article } from '../models/articles';
import { Category } from '../models/categories';
import { getUuids } from './mock-uuids';

const uuids = getUuids();

console.log(uuids);

export const categoriesData: Category[] = [
  { id: uuids.catIds[0], title: 'Lame Articles' },
  { id: uuids.catIds[1], title: 'Cool Articles' }
];

export const articlesData: Article[] = [
  {
    id: uuids.artIds[0],
    order: 0,
    rootArticle: true,
    title: 'Getting started',
    content: `<p>Using ECO Assist, customer service representatives (CSRs)
        can troubleshoot service issues for individual subscribers by viewing
        information about the health of each service, including diagnostics and
        service parameters. ECO Assist also provides information about devices
        associated with services, and other devices within the subscriber’s network.</p>`,
    categoryId: uuids.catIds[0],
    application: [{ 'ECO Assist': ['2.0'] }],
    childrenIds: [uuids.artIds[1]]
  },
  {
    id: uuids.artIds[1],
    order: 0,
    rootArticle: false,
    title: 'User permissions',
    content: `<p>Using ECO Assist, customer service representatives (CSRs)
        can troubleshoot service issues for individual subscribers by viewing
        information about the health of each service, including diagnostics and
        service parameters. ECO Assist also provides information about devices
        associated with services, and other devices within the subscriber’s network.</p>`,
    categoryId: uuids.catIds[1],
    application: [{ 'ECO Assist': ['2.0'] }],
    childrenIds: []
  },
  {
    id: uuids.artIds[2],
    order: 1,
    rootArticle: true,
    title: 'Do Something Else',
    content: `<p>Using ECO Assist, customer service representatives (CSRs)
        can troubleshoot service issues for individual subscribers by viewing
        information about the health of each service, including diagnostics and
        service parameters. ECO Assist also provides information about devices
        associated with services, and other devices within the subscriber’s network.</p>`,
    categoryId: uuids.catIds[0],
    application: [{ 'ECO Assist': ['2.0'] }],
    childrenIds: []
  },
  {
    id: uuids.artIds[3],
    order: 2,
    rootArticle: true,
    title: 'Do Something Different',
    content: `<p>Using ECO Assist, customer service representatives (CSRs)
        can troubleshoot service issues for individual subscribers by viewing
        information about the health of each service, including diagnostics and
        service parameters. ECO Assist also provides information about devices
        associated with services, and other devices within the subscriber’s network.</p>`,
    categoryId: '',
    application: [{ 'ECO Assist': ['2.0'] }],
    childrenIds: []
  }
];
