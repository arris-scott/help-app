import { InMemoryDbService } from 'angular-in-memory-web-api';

import { applicationsData, getAppPreviews } from './applications-data';
import { articlesData, categoriesData } from './articles-data';
// TODO: Learn to parse InMemoryDbService requests
export class AppData implements InMemoryDbService {
  createDb() {
    const articles = articlesData;
    const categories = categoriesData;
    const applications = applicationsData;
    const appPreviews = getAppPreviews();
    return { articles, categories, applications, appPreviews };
  }
}
