import { Application, AppPreview } from '../models/application';
import { getUuids } from './mock-uuids';

const uuids = getUuids();

console.log(uuids);

export const applicationsData: Application[] = [
  {
    id: uuids.appIds[0],
    title: 'Eco Assist',
    versions: [
      {
        id: '1.0',
        articles: [
          {
            id: uuids.artIds[0],
            order: 0,
            root: true,
            children: [
              uuids.artIds[1]
            ]
          },
          {
            id: uuids.artIds[1],
            order: 0,
            root: false,
            children: []
          },
          {
            id: uuids.artIds[2],
            order: 1,
            root: true,
            children: []
          },
          {
            id: uuids.artIds[3],
            order: 2,
            root: true,
            children: []
          },
        ]
      },
      {
        id: '2.0',
        articles: [
          {
            id: uuids.artIds[0],
            order: 0,
            root: true,
            children: [
              uuids.artIds[1]
            ]
          },
          {
            id: uuids.artIds[1],
            order: 0,
            root: false,
            children: []
          },
          {
            id: uuids.artIds[2],
            order: 1,
            root: true,
            children: []
          }
        ]
      }
    ]
  },
  {
    id: uuids.appIds[0],
    title: 'Eco Manage',
    versions: [
      {
        id: '1.0',
        articles: [
          {
            id: uuids.artIds[0],
            order: 0,
            root: true,
            children: [
              uuids.artIds[1]
            ]
          },
          {
            id: uuids.artIds[1],
            order: 0,
            root: false,
            children: []
          },
          {
            id: uuids.artIds[2],
            order: 1,
            root: true,
            children: []
          },
          {
            id: uuids.artIds[3],
            order: 2,
            root: true,
            children: []
          },
        ]
      },
      {
        id: '2.0',
        articles: [
          {
            id: uuids.artIds[0],
            order: 0,
            root: true,
            children: [
              uuids.artIds[1]
            ]
          },
          {
            id: uuids.artIds[1],
            order: 0,
            root: false,
            children: []
          },
          {
            id: uuids.artIds[2],
            order: 1,
            root: true,
            children: []
          }
        ]
      },
      {
        id: '3.0',
        articles: [
          {
            id: uuids.artIds[0],
            order: 0,
            root: true,
            children: []
          },
          {
            id: uuids.artIds[1],
            order: 2,
            root: true,
            children: []
          },
          {
            id: uuids.artIds[2],
            order: 1,
            root: true,
            children: []
          }
        ]
      }
    ]
  },
  {
    id: uuids.appIds[0],
    title: 'Eco Control',
    versions: [
      {
        id: '1.0',
        articles: [
          {
            id: uuids.artIds[0],
            order: 0,
            root: true,
            children: [
              uuids.artIds[1]
            ]
          },
          {
            id: uuids.artIds[1],
            order: 0,
            root: false,
            children: []
          },
          {
            id: uuids.artIds[2],
            order: 1,
            root: true,
            children: []
          },
          {
            id: uuids.artIds[3],
            order: 2,
            root: true,
            children: []
          },
        ]
      },
      {
        id: '2.0',
        articles: [
          {
            id: uuids.artIds[0],
            order: 0,
            root: true,
            children: []
          },
          {
            id: uuids.artIds[1],
            order: 2,
            root: true,
            children: []
          },
          {
            id: uuids.artIds[2],
            order: 1,
            root: true,
            children: []
          }
        ]
      }
    ]
  }
];

export const getAppPreviews = (): AppPreview[] => applicationsData.map(app => Object({
  id: app.id,
  title: app.title,
  versions: app.versions.map(version => version.id)
}));

export const getApps = (): AppPreview[] => applicationsData.map(app => Object({
  id: app.id,
  title: app.title,
  versions: app.versions.map(version => version.id)
}));
