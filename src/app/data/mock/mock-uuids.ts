import { UUID } from 'angular2-uuid';

function genUuids() {
  const catIds = [
    UUID.UUID(),
    UUID.UUID()
  ];

  const artIds = [
    UUID.UUID(),
    UUID.UUID(),
    UUID.UUID(),
    UUID.UUID()
  ];

  const appIds = [
    UUID.UUID(),
    UUID.UUID(),
    UUID.UUID(),
    UUID.UUID()
  ];

  const uuids = {
    catIds,
    artIds,
    appIds
  };

  return () => uuids;
}

export const getUuids = genUuids();
