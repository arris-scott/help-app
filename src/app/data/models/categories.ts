import { UUID } from 'angular2-uuid';

export type CategoryUserActions = 'create' | 'delete' | 'update';

export interface CategoryAction {
  action: CategoryUserActions;
  payload: string | number;
}

export class Category {
  constructor(
    public title: string,
    public id?: string
  ) {
    if (!this.id) {
      this.id = UUID.UUID();
    }
  }
}
