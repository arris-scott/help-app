export class Application {
  constructor (
    public id: string,
    public title: string,
    public versions: Version[],
  ) {}
}

export interface Version {
  id: string;
  articles: ArticleRef[];
}

export interface ArticleRef {
  id: string;
  order: number;
  root: boolean;
  children: string[];
}

export interface AppPreview {
  id: string;
  title: string;
  versions: string[];
}
