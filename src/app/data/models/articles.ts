export class Article {
  constructor(
    public id: string,
    public order: number,
    public rootArticle: boolean,
    public title: string,
    public content: string,
    public categoryId: string,
    public application: { [k: string]: string[] }[],
    public childrenIds: string[]) { }
}
