import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { from, Observable, throwError } from 'rxjs';
import { catchError, map, mergeMap, tap } from 'rxjs/operators';

import { Article } from '../models/articles';
import { Category } from '../models/categories';
import { AppPreview, Application, Version } from '../models/application';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  private articlesUrl = 'api/articles';
  private categoriesUrl = 'api/categories';
  private applicationsUrl = 'api/applications';
  private appPreviewsUrl = 'api/appPreviews';

  constructor(private http: HttpClient) { }

  getAppPreviews(): Observable<AppPreview[]> {
    return this.http.get<AppPreview[]>(this.appPreviewsUrl).pipe(
      tap(data => console.log(JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  getAppVersion(appId: string, versionId: string): Observable<Version> {
    const url = `${this.applicationsUrl}/${appId}/versions/${versionId}`;
    return this.http.get<Version>(url).pipe(
      tap(data => console.log(JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  getApplication(id: string): Observable<Application> {
    const url = `${this.applicationsUrl}/${id}`;
    return this.http.get<Application>(url).pipe(
      catchError(this.handleError)
    );
  }

  createApplication(application: Application): Observable<Application> {
    return this.http.post<Application>(this.applicationsUrl, application, this.newHttpHeaders())
      .pipe(
        catchError(this.handleError)
      );
  }

  updateApplication(application: Application): Observable<Application> {
    const url = `${this.articlesUrl}/${application.id}`;
    return this.http.put<Application>(url, application, this.newHttpHeaders())
      .pipe(
        tap(() => console.log('updateArticle: ' + application.id)),
        map(() => application),
        catchError(this.handleError)
      );
  }

  deleteApplication(id: string): Observable<{}> {
    const url = `${this.applicationsUrl}/${id}`;
    return this.http.delete<Application>(url, this.newHttpHeaders())
      .pipe(
        catchError(this.handleError)
      );
  }

  getArticles(): Observable<Article[]> {
    return this.http.get<Article[]>(this.articlesUrl)
      .pipe(
        tap(data => console.log(JSON.stringify(data))),
        catchError(this.handleError)
      );
  }

  createArticle(article: Article): Observable<Article> {
    return this.http.post<Article>(this.articlesUrl, article, this.newHttpHeaders())
      .pipe(
        catchError(this.handleError)
      );
  }

  updateArticle(article: Article): Observable<Article> {
    const url = `${this.articlesUrl}/${article.id}`;
    return this.http.put<Article>(url, article, this.newHttpHeaders())
      .pipe(
        tap(() => console.log('updateArticle: ' + article.id)),
        map(() => article),
        catchError(this.handleError)
      );
  }

  updateArticles(articles: Article[]): Observable<Article> {
    return from(articles).pipe(
      mergeMap(article => this.updateArticle(article)),
      catchError(this.handleError)
    );
  }

  deleteArticle(id: string): Observable<{}> {
    const url = `${this.articlesUrl}/${id}`;
    return this.http.delete<Article>(url, this.newHttpHeaders())
      .pipe(
        catchError(this.handleError)
      );
  }

  getCategories(): Observable<{}> {
    return this.http.get<Category[]>(this.categoriesUrl)
      .pipe(
        tap(data => console.log(JSON.stringify(data))),
        catchError(this.handleError)
      );
  }

  createCategory(category: Category): Observable<Category> {
    return this.http.post<Category>(this.categoriesUrl, category, this.newHttpHeaders())
      .pipe(
        catchError(this.handleError)
      );
  }

  updateCategory(category: Category): Observable<Category> {
    const url = `${this.categoriesUrl}/${category.id}`;
    return this.http.put<Category>(url, category, this.newHttpHeaders())
      .pipe(
        tap(() => console.log('updateCategory: ' + category.id)),
        map(() => category),
        catchError(this.handleError)
      );
  }

  deleteCategory(id: string): Observable<{}> {
    const url = `${this.categoriesUrl}/${id}`;
    return this.http.delete<Category>(url, this.newHttpHeaders())
      .pipe(
        catchError(this.handleError)
      );
  }

  private newHttpHeaders(header = { 'Content-Type': 'application/json' }) {
    const headers = new HttpHeaders(header);
    return { headers };
  }

  private handleError(err) {
    let errorMessage: string;
    if (err.error instanceof ErrorEvent) {
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      errorMessage = `Backend returned code ${err.status}: ${err.body.error}`;
    }
    console.error(err);
    return throwError(errorMessage);
  }
}
