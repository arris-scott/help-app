import { Action } from '@ngrx/store';
import { Application, AppPreview } from '../../models/application';

export enum ApplicationActionTypes {
  LoadAppPreviews = '[Application] Load App Previews',
  LoadAppPreviewsSuccess = '[Application] Load App Previews Success',
  LoadAppPreviewsFail = '[Application] Load App Previews Fail',
  LoadApplication = '[Application] Load Application',
  LoadApplicationSuccess = '[Application] Load Application Success',
  LoadApplicationFail = '[Application] Load Application Fail',
  CreateApplication = '[Application] Create Application',
  CreateApplicationSuccess = '[Application] Create Application Success',
  CreateApplicationFail = '[Application] Create Application Fail',
  UpdateApplication = '[Application] Update Application',
  UpdateApplicationSuccess = '[Application] Update Application Success',
  UpdateApplicationFail = '[Application] Update Application Fail',
  DeleteApplication = '[Application] Delete Application',
  DeleteApplicationSuccess = '[Application] Delete Application Success',
  DeleteApplicationFail = '[Application] Delete Application Fail'
}


export class LoadAppPreviews implements Action {
  readonly type = ApplicationActionTypes.LoadAppPreviews;
}
export class LoadAppPreviewsSuccess implements Action {
  readonly type = ApplicationActionTypes.LoadAppPreviewsSuccess;
  constructor(public payload: AppPreview[]) { }
}
export class LoadAppPreviewsFail implements Action {
  readonly type = ApplicationActionTypes.LoadAppPreviewsFail;
  constructor(public payload: string) { }
}
export class LoadApplication implements Action {
  readonly type = ApplicationActionTypes.LoadApplication;
  constructor(public payload: {}) { }
}
export class LoadApplicationSuccess implements Action {
  readonly type = ApplicationActionTypes.LoadApplicationSuccess;
  constructor(public payload: Application) { }
}
export class LoadApplicationFail implements Action {
  readonly type = ApplicationActionTypes.LoadApplicationFail;
  constructor(public payload: string) { }
}
export class CreateApplication implements Action {
  readonly type = ApplicationActionTypes.CreateApplication;
  constructor(public payload: Application) { }
}
export class CreateApplicationSuccess implements Action {
  readonly type = ApplicationActionTypes.CreateApplicationSuccess;
  constructor(public payload: Application) { }
}
export class CreateApplicationFail implements Action {
  readonly type = ApplicationActionTypes.CreateApplicationFail;
  constructor(public payload: string) { }
}
export class UpdateApplication implements Action {
  readonly type = ApplicationActionTypes.UpdateApplication;
  constructor(public payload: Application) { }
}
export class UpdateApplicationSuccess implements Action {
  readonly type = ApplicationActionTypes.UpdateApplicationSuccess;
  constructor(public payload: Application) { }
}
export class UpdateApplicationFail implements Action {
  readonly type = ApplicationActionTypes.UpdateApplicationFail;
  constructor(public payload: string) { }
}
export class DeleteApplication implements Action {
  readonly type = ApplicationActionTypes.DeleteApplication;
  constructor(public payload: string) { }
}
export class DeleteApplicationSuccess implements Action {
  readonly type = ApplicationActionTypes.DeleteApplicationSuccess;
  constructor(public payload: string) { }
}
export class DeleteApplicationFail implements Action {
  readonly type = ApplicationActionTypes.DeleteApplicationFail;
  constructor(public payload: string) { }
}

export type ApplicationActions
  = LoadAppPreviews
  | LoadAppPreviewsSuccess
  | LoadAppPreviewsFail
  | LoadApplication
  | LoadApplicationSuccess
  | LoadApplicationFail
  | CreateApplication
  | CreateApplicationSuccess
  | CreateApplicationFail
  | UpdateApplication
  | UpdateApplicationSuccess
  | UpdateApplicationFail
  | DeleteApplication
  | DeleteApplicationSuccess
  | DeleteApplicationFail;
