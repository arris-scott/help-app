import { Application, AppPreview } from '../../models/application';
import { ApplicationActions, ApplicationActionTypes } from './application.actions';

export interface ApplicationState {
  applications: AppPreview[];
  application: Application;
  error: string;
}

export const initialState: ApplicationState = {
  applications: [],
  application: undefined,
  error: ''
};

export function reducer(state: ApplicationState = initialState, action: ApplicationActions): ApplicationState {
  switch (action.type) {

    case ApplicationActionTypes.LoadAppPreviewsSuccess: {
      const applications = action.payload;
      return {
        ...state,
        applications,
        error: ''
      };
    }
    case ApplicationActionTypes.LoadAppPreviewsFail:
      return {
        ...state,
        applications: [],
        error: action.payload
      };
    case ApplicationActionTypes.LoadApplicationSuccess: {
      return {
        ...state,
        application: action.payload,
        error: ''
      };
    }
    case ApplicationActionTypes.LoadApplicationFail:
      return {
        ...state,
        applications: [],
        error: action.payload
      };

    case ApplicationActionTypes.CreateApplicationSuccess:
      return {
        ...state,
        application: action.payload,
        error: ''
      };
    case ApplicationActionTypes.CreateApplicationFail:
      return {
        ...state,
        error: action.payload
      };

    case ApplicationActionTypes.UpdateApplicationSuccess: {
      return {
        ...state,
        application: action.payload,
        error: ''
      };
    }
    case ApplicationActionTypes.UpdateApplicationFail:
      return {
        ...state,
        error: action.payload
      };

    case ApplicationActionTypes.DeleteApplicationSuccess: {
      return {
        ...state,
        application: undefined,
        error: ''
      };
    }
    case ApplicationActionTypes.DeleteApplicationFail:
      return {
        ...state,
        error: action.payload
      };

    default:
      return state;
  }
}
