import { Injectable } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { Actions, Effect, ofType } from '@ngrx/effects';

import * as fromRoot from '../../../state/app.state';
import { Store, Action } from '@ngrx/store';
import * as applicationActions from './application.actions';
import * as articlesActions from '../articles/articles.actions';
import { Observable, of } from 'rxjs';
import { mergeMap, map, catchError, switchMap, concatMap } from 'rxjs/operators';
import { AppPreview, Application } from '../../models/application';


@Injectable()
export class ApplicationEffects {
  constructor(
    private actions$: Actions,
    private httpService: HttpService,
    private store: Store<fromRoot.State>
  ) { }

  @Effect()
  loadAppPreviews$: Observable<Action> = this.actions$.pipe(
    ofType(applicationActions.ApplicationActionTypes.LoadAppPreviews),
    mergeMap(() => this.httpService.getAppPreviews().pipe(
      map((appPreviews: AppPreview[]) => new applicationActions.LoadAppPreviewsSuccess(appPreviews)),
      catchError(err => of(new applicationActions.LoadAppPreviewsFail(err)))
    ))
  );

  // @Effect()
  // loadApplication$: Observable<Action> = this.actions$.pipe(
  //   ofType(applicationActions.ApplicationActionTypes.LoadApplication),
  //   map((action: applicationActions.LoadApplication) => action.payload),
  //   mergeMap((appId: string) => this.httpService.getApplication(appId).pipe(
  //     mergeMap((application: Application) => {
  //       const articles = application.
  //     }),
  //     // switchMap((application: Application) => [
  //     //   new applicationActions.LoadApplicationSuccess(application),
  //     //   new articlesActions.Load()
  //     // ]),
  //     catchError(err => of(new applicationActions.LoadApplicationFail(err)))
  //   ))
  // );

  // @Effect()
  // createApplication$: Observable<Action> = this.actions$.pipe(
  //   ofType(applicationActions.ApplicationActionTypes.CreateApplication),

  // )

}
