import { Action } from '@ngrx/store';

import { Category } from '../../models/categories';

export enum CategoriesActionTypes {
  Load = '[Categories] Load',
  LoadSuccess = '[Categories] Load Success',
  LoadFail = '[Categories] Load Fail',
  UpdateCategory = '[Categories] Update Category',
  UpdateCategorySuccess = '[Categories] Update Category Success',
  UpdateCategoryFail = '[Categories] Update Category Fail',
  CreateCategory = '[Categories] Create Category',
  CreateCategorySuccess = '[Categories] Create Category Success',
  CreateCategoryFail = '[Categories] Create Category Fail',
  DeleteCategory = '[Categories] Delete Category',
  DeleteCategorySuccess = '[Categories] Delete Category Success',
  DeleteCategoryFail = '[Categories] Delete Category Fail'
}

export class Load implements Action {
  readonly type = CategoriesActionTypes.Load;
  constructor() { }
}

export class LoadSuccess implements Action {
  readonly type = CategoriesActionTypes.LoadSuccess;
  constructor(public payload: Category[]) { }
}

export class LoadFail implements Action {
  readonly type = CategoriesActionTypes.LoadFail;
  constructor(public payload: string) { }
}

export class UpdateCategory implements Action {
  readonly type = CategoriesActionTypes.UpdateCategory;
  constructor(public payload: Category) { }
}
export class UpdateCategorySuccess implements Action {
  readonly type = CategoriesActionTypes.UpdateCategorySuccess;
  constructor(public payload: Category) { }
}
export class UpdateCategoryFail implements Action {
  readonly type = CategoriesActionTypes.UpdateCategoryFail;
  constructor(public payload: string) { }
}
export class CreateCategory implements Action {
  readonly type = CategoriesActionTypes.CreateCategory;
  constructor(public payload: Category) { }
}
export class CreateCategorySuccess implements Action {
  readonly type = CategoriesActionTypes.CreateCategorySuccess;
  constructor(public payload: Category) { }
}
export class CreateCategoryFail implements Action {
  readonly type = CategoriesActionTypes.CreateCategoryFail;
  constructor(public payload: string) { }
}
export class DeleteCategory implements Action {
  readonly type = CategoriesActionTypes.DeleteCategory;
  constructor(public payload: string) { }
}
export class DeleteCategorySuccess implements Action {
  readonly type = CategoriesActionTypes.DeleteCategorySuccess;
  constructor(public payload: string) { }
}
export class DeleteCategoryFail implements Action {
  readonly type = CategoriesActionTypes.DeleteCategoryFail;
  constructor(public payload: string) { }
}

export type CategoriesActions
  = Load
  | LoadSuccess
  | LoadFail
  | CreateCategory
  | CreateCategorySuccess
  | CreateCategoryFail
  | UpdateCategory
  | UpdateCategorySuccess
  | UpdateCategoryFail
  | DeleteCategory
  | DeleteCategorySuccess
  | DeleteCategoryFail;
