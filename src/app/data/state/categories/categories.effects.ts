import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action, select, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, first, map, mergeMap, switchMap } from 'rxjs/operators';

import * as fromRoot from '../../../state/app.state';
import * as fromData from '..';
import { Article } from '../../models/articles';
import { Category } from '../../models/categories';
import { HttpService } from '../../services/http.service';
import * as articlesActions from '../articles/articles.actions';
import * as categoriesActions from './categories.actions';

@Injectable()
export class CategoriesEffects {
  constructor(
    private actions$: Actions,
    private httpService: HttpService,
    private store: Store<fromRoot.State>
  ) { }

  @Effect()
  loadCategories$: Observable<Action> = this.actions$
    .pipe(
      ofType(categoriesActions.CategoriesActionTypes.Load),
      mergeMap(() => this.httpService.getCategories()
        .pipe(
          map((categories: Category[]) => new categoriesActions.LoadSuccess(categories)),
          catchError(err => of(new categoriesActions.LoadFail(err)))
        )
      )
    );

  @Effect()
  createCategory$: Observable<Action> = this.actions$
    .pipe(
      ofType(categoriesActions.CategoriesActionTypes.CreateCategory),
      map((action: categoriesActions.CreateCategory) => action.payload),
      mergeMap((category: Category) => this.httpService.createCategory(category)
        .pipe(
          map((newCategory: Category) => new categoriesActions.CreateCategorySuccess(newCategory)),
          catchError(err => of(new categoriesActions.CreateCategoryFail(err)))
        )
      )
    );

  @Effect()
  updateCategory$: Observable<Action> = this.actions$
    .pipe(
      ofType(categoriesActions.CategoriesActionTypes.UpdateCategory),
      map((action: categoriesActions.UpdateCategory) => action.payload),
      mergeMap((category: Category) => this.httpService.updateCategory(category)
        .pipe(
          map((updatedCategory: Category) => new categoriesActions.UpdateCategorySuccess(updatedCategory)),
          catchError(err => of(new categoriesActions.UpdateCategoryFail(err)))
        )
      )
    );

  @Effect()
  deleteCategory$: Observable<Action> = this.actions$.pipe(
    ofType(categoriesActions.CategoriesActionTypes.DeleteCategory),
    map((action: categoriesActions.DeleteCategory) => action.payload),
    mergeMap((id: string) => this.httpService.deleteCategory(id).pipe(
      switchMap(() => this.store.pipe(
        select(fromData.getArticlesByCategory, { categoryId: id }),
        first(),
        mergeMap((articles: Article[]) => {
          if (articles.length) {
            const articleActions = articles
              .map(article => new articlesActions.UpdateArticle({ ...article, categoryId: '' }));
            return [
              new categoriesActions.DeleteCategorySuccess(id),
              ...articleActions
            ];
          }
          return [new categoriesActions.DeleteCategorySuccess(id)];
        })
      )),
      catchError(err => of(new categoriesActions.DeleteCategoryFail(err)))
    ))
  );
}
