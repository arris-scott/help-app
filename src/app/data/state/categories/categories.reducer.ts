import { Category } from '../../models/categories';
import { CategoriesActions, CategoriesActionTypes } from './categories.actions';

import * as fromData from '..';

export interface CategoriesState {
  categories: Category[];
  error: string;
}

export const initialState: CategoriesState = {
  categories: [],
  error: ''
};

export function reducer(state: CategoriesState = initialState, action: CategoriesActions): CategoriesState {
  switch (action.type) {

    case CategoriesActionTypes.LoadSuccess:
      return {
        ...state,
        categories: action.payload,
        error: ''
      };
    case CategoriesActionTypes.LoadFail:
      return {
        ...state,
        categories: [],
        error: action.payload
      };

    case CategoriesActionTypes.CreateCategorySuccess:
      return {
        ...state,
        categories: [...state.categories, action.payload],
        error: ''
      };
    case CategoriesActionTypes.CreateCategoryFail:
      return {
        ...state,
        error: action.payload
      };

    case CategoriesActionTypes.UpdateCategorySuccess: {
      const categories = state.categories
      .map(category => category.id === action.payload.id ? action.payload : category);
      return {
        ...state,
        categories,
        error: ''
      };
    }
    case CategoriesActionTypes.UpdateCategoryFail:
      return {
        ...state,
        error: action.payload
      };

    case CategoriesActionTypes.DeleteCategorySuccess: {
      const categories = state.categories
        .reduce((updatedCats, category) => {
          return category.id === action.payload ? updatedCats : [...updatedCats, category];
        }, []);
      return {
        ...state,
        categories,
        error: ''
      };
    }
    case CategoriesActionTypes.DeleteCategoryFail:
      return {
        ...state,
        error: action.payload
      };

    default:
      return state;
  }
}
