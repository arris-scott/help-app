import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action, select, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, first, map, mergeMap, switchMap } from 'rxjs/operators';

import * as fromShared from '../../../shared/state';
import * as sharedActions from '../../../shared/state/shared.actions';
import * as fromRoot from '../../../state/app.state';
import { Article } from '../../models/articles';
import { HttpService } from '../../services/http.service';
import * as articlesActions from './articles.actions';

@Injectable()
export class ArticlesEffects {
  constructor(
    private actions$: Actions,
    private httpService: HttpService,
    private store: Store<fromRoot.State>
  ) { }

  @Effect()
  loadArticles$: Observable<Action> = this.actions$.pipe(
    ofType(articlesActions.ArticlesActionTypes.Load),
    mergeMap((action: articlesActions.Load) => this.httpService.getArticles().pipe(
      map((articles: Article[]) => new articlesActions.LoadSuccess(articles)),
      catchError(err => of(new articlesActions.LoadFail(err)))
    ))
  );
  // @Effect()
  // loadArticles$: Observable<Action> = this.actions$.pipe(
  //   ofType(articlesActions.ArticlesActionTypes.Load),
  //   mergeMap((action: articlesActions.Load) => this.httpService.getArticles().pipe(
  //     switchMap((articles: Article[]) => [
  //       new articlesActions.LoadSuccess(articles),
  //       new sharedActions.SetSelectedArticleId(articles[0].id)
  //     ]),
  //     catchError(err => of(new articlesActions.LoadFail(err)))
  //   ))
  // );

  @Effect()
  createArticle$: Observable<Action> = this.actions$.pipe(
    ofType(articlesActions.ArticlesActionTypes.CreateArticle),
    map((action: articlesActions.CreateArticle) => action.payload),
    mergeMap((article: Article) => this.httpService.createArticle(article).pipe(
      map((newArticle: Article) => new articlesActions.CreateArticleSuccess(newArticle)),
      catchError(err => of(new articlesActions.CreateArticleFail(err)))
    ))
  );

  @Effect()
  updateArticle$: Observable<Action> = this.actions$.pipe(
    ofType(articlesActions.ArticlesActionTypes.UpdateArticle),
    map((action: articlesActions.UpdateArticle) => action.payload),
    mergeMap((article: Article) => this.httpService.updateArticle(article).pipe(
      map((updatedArticle: Article) => new articlesActions.UpdateArticleSuccess(updatedArticle)),
      catchError(err => of(new articlesActions.UpdateArticleFail(err)))
    ))
  );

  @Effect()
  deleteArticle$: Observable<Action> = this.actions$.pipe(
    ofType(articlesActions.ArticlesActionTypes.DeleteArticle),
    map((action: articlesActions.DeleteArticle) => action.payload),
    mergeMap((articleId: string) => this.httpService.deleteArticle(articleId).pipe(
      switchMap((deletedArticleId: string) =>  this.store.pipe(
        select(fromShared.getSelectedArticleId),
        first(),
        switchMap((selectedArticleId: string) => {
          if (selectedArticleId === deletedArticleId) {
            return [
              new articlesActions.DeleteArticleSuccess(deletedArticleId),
              new sharedActions.SetSelectedArticleId('')
            ];
          }
          return [new articlesActions.DeleteArticleSuccess(deletedArticleId)];
        }))
      ),
      catchError(err => of(new articlesActions.DeleteArticleFail(err)))
    ))
  );
}
