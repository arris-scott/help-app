import { Article } from '../../../data/models/articles';
import { ArticlesActions, ArticlesActionTypes } from './articles.actions';

import * as fromData from '..';

export interface ArticlesState {
  articles: Article[];
  error: string;
}
export const initialState: ArticlesState = {
  articles: [],
  error: ''
};

export function reducer(state: ArticlesState = initialState, action: ArticlesActions): ArticlesState {
  switch (action.type) {

    case ArticlesActionTypes.LoadSuccess: {
      const articles = action.payload;
      return {
        ...state,
        articles,
        error: ''
      };
    }
    case ArticlesActionTypes.LoadFail:
      return {
        ...state,
        articles: [],
        error: action.payload
      };

    case ArticlesActionTypes.CreateArticleSuccess:
      return {
        ...state,
        articles: [...state.articles, action.payload],
        error: ''
      };
    case ArticlesActionTypes.CreateArticleFail:
      return {
        ...state,
        error: action.payload
      };

    case ArticlesActionTypes.UpdateArticleSuccess: {
      const articles = state.articles
        .map(article => article.id === action.payload.id ? action.payload : article);
      return {
        ...state,
        articles,
        error: ''
      };
    }
    case ArticlesActionTypes.UpdateArticleFail:
      return {
        ...state,
        error: action.payload
      };

    case ArticlesActionTypes.DeleteArticleSuccess: {
      const articles = state.articles
        .reduce((updatedArts, article) => {
          return article.id === action.payload ? updatedArts : [...updatedArts, article];
        }, []);
      return {
        ...state,
        articles,
        error: ''
      };
    }
    case ArticlesActionTypes.DeleteArticleFail:
      return {
        ...state,
        error: action.payload
      };

    default:
      return state;
  }
}
