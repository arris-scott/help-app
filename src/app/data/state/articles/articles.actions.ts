import { Action } from '@ngrx/store';

import { Article } from '../../models/articles';

export enum ArticlesActionTypes {
  Load = '[Articles] Load',
  LoadSuccess = '[Articles] Load Success',
  LoadFail = '[Articles] Load Fail',
  CreateArticle = '[Articles] Create Article',
  CreateArticleSuccess = '[Articles] Create Article Success',
  CreateArticleFail = '[Articles] Create Article Fail',
  UpdateArticle = '[Articles] Update Article',
  UpdateArticleSuccess = '[Articles] Update Article Success',
  UpdateArticleFail = '[Articles] Update Article Fail',
  DeleteArticle = '[Articles] Delete Article',
  DeleteArticleSuccess = '[Articles] Delete Article Success',
  DeleteArticleFail = '[Articles] Delete Article Fail'
}

export class Load implements Action {
  readonly type = ArticlesActionTypes.Load;
}
export class LoadSuccess implements Action {
  readonly type = ArticlesActionTypes.LoadSuccess;
  constructor(public payload: Article[]) { }
}
export class LoadFail implements Action {
  readonly type = ArticlesActionTypes.LoadFail;
  constructor(public payload: string) { }
}

export class CreateArticle implements Action {
  readonly type = ArticlesActionTypes.CreateArticle;
  constructor(public payload: Article) { }
}
export class CreateArticleSuccess implements Action {
  readonly type = ArticlesActionTypes.CreateArticleSuccess;
  constructor(public payload: Article) { }
}
export class CreateArticleFail implements Action {
  readonly type = ArticlesActionTypes.CreateArticleFail;
  constructor(public payload: string) { }
}

export class UpdateArticle implements Action {
  readonly type = ArticlesActionTypes.UpdateArticle;
  constructor(public payload: Article) { }
}
export class UpdateArticleSuccess implements Action {
  readonly type = ArticlesActionTypes.UpdateArticleSuccess;
  constructor(public payload: Article) { }
}
export class UpdateArticleFail implements Action {
  readonly type = ArticlesActionTypes.UpdateArticleFail;
  constructor(public payload: string) { }
}

export class DeleteArticle implements Action {
  readonly type = ArticlesActionTypes.DeleteArticle;
  constructor(public payload: string) { }
}
export class DeleteArticleSuccess implements Action {
  readonly type = ArticlesActionTypes.DeleteArticleSuccess;
  constructor(public payload: string) { }
}
export class DeleteArticleFail implements Action {
  readonly type = ArticlesActionTypes.DeleteArticleFail;
  constructor(public payload: string) { }
}

export type ArticlesActions
  = Load
  | LoadSuccess
  | LoadFail
  | CreateArticle
  | CreateArticleSuccess
  | CreateArticleFail
  | UpdateArticle
  | UpdateArticleSuccess
  | UpdateArticleFail
  | DeleteArticle
  | DeleteArticleSuccess
  | DeleteArticleFail;
