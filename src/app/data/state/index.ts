import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';
import reduceReducers from 'reduce-reducers';

import { Article } from '../models/articles';
import * as fromArticles from './articles/articles.reducer';
import * as fromCategories from './categories/categories.reducer';
import * as fromApplication from './application/application.reducer';

export interface State extends fromArticles.ArticlesState, fromCategories.CategoriesState { }

const initialState = {
  ...fromArticles.initialState,
  ...fromCategories.initialState,
  ...fromApplication.initialState
};

export const reducer: ActionReducerMap<State> = reduceReducers(
  fromArticles.reducer,
  fromCategories.reducer,
  fromApplication.reducer,
  initialState
);

const getDataFeatureState = createFeatureSelector<State>('data');

export const getArticles = createSelector(
  getDataFeatureState,
  (state): Article[] => state.articles.sort((a, b) => a.title > b.title ? 1 : -1)
);

export const getCategories = createSelector(
  getDataFeatureState,
  state => state.categories.sort((a, b) => a.title > b.title ? 1 : -1)
);

export const getErrorMessage = createSelector(
  getDataFeatureState,
  state => state.error
);

export const getArticleById = createSelector(
  getArticles,
  (state, props: { id: string }) => state.find(article => article.id === props.id)
);

export const getArticlesByCategory = createSelector(
  getArticles,
  (state: Article[], props: { categoryId: string }) => state
    .filter(article => article.categoryId === props.categoryId)
);
