import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { TreeModule } from 'angular-tree-component';
import { environment } from 'src/environments/environment.prod';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ArticleExplorerModule } from './article-explorer/article-explorer.module';
import { ArticleModule } from './article/article.module';
import { CategoryModalModule } from './category-modal/category-modal.module';
import {
  CategoryModalContainerComponent,
} from './category-modal/containers/category-modal-container/category-modal-container.component';
import { CoreModule } from './core/core.module';
import { AppData } from './data/mock/app-data';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(AppData),
    NgbModule,
    StoreModule.forRoot({}),
    EffectsModule.forRoot([]),
    StoreDevtoolsModule.instrument({
      name: 'Help App',
      maxAge: 25,
      logOnly: environment.production
    }),
    CoreModule,
    ArticleModule,
    ArticleExplorerModule,
    CategoryModalModule,
    TreeModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [CategoryModalContainerComponent]
})
export class AppModule { }
