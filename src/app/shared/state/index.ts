import { createFeatureSelector, createSelector } from '@ngrx/store';

import { State } from './shared.reducer';

const getSharedFeatureState = createFeatureSelector<State>('shared');

export const getSearchTerm = createSelector(
  getSharedFeatureState,
  state => state.searchTerm
);

export const getSelectedArticleId = createSelector(
  getSharedFeatureState,
  state => state.selectedArticleId
);
