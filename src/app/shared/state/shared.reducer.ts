import { SharedActions, SharedActionTypes } from './shared.actions';

export interface State {
  searchTerm: string;
  selectedArticleId: string;
}

const initState: State = {
  searchTerm: '',
  selectedArticleId: ''
};

export function reducer(state: State = initState, action: SharedActions): State {
  switch (action.type) {
    case SharedActionTypes.SetSearchTerm:
      return {
        ...state,
        searchTerm: action.payload
      };
    case SharedActionTypes.SetSelectedArticleId:
      return {
        ...state,
        selectedArticleId: action.payload
      };
    default:
      return state;
  }
}
