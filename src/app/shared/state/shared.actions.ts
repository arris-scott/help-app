import { Action } from '@ngrx/store';

export enum SharedActionTypes {
  SetSearchTerm = '[Shared] Set Search Term',
  SetSelectedArticleId = '[Shared] Set Selected Article ID'
}

export class SetSearchTerm implements Action {
  readonly type = SharedActionTypes.SetSearchTerm;
  constructor(public payload: string) { }
}
export class SetSelectedArticleId implements Action {
  readonly type = SharedActionTypes.SetSelectedArticleId;
  constructor(public payload: string) { }
}

export type SharedActions
  = SetSearchTerm
  | SetSelectedArticleId;
