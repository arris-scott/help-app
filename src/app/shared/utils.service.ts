import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class Utils {

  equalJson(a, b): boolean {
    return JSON.stringify(a) === JSON.stringify(b);
  }

  jsonDeepCopy<T>(obj: T): T {
    return JSON.parse(JSON.stringify(obj));
  }
}
