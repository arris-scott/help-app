import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as fromRoot from '../../state/app.state';
import * as fromShared from '../state';
import * as sharedActions from '../state/shared.actions';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  @Output() search = new EventEmitter<string>();

  searchTerm$: Observable<string>;

  faSearch = faSearch;

  constructor(private store: Store<fromRoot.State>) { }

  ngOnInit() {
    this.searchTerm$ = this.store.pipe(select(fromShared.getSearchTerm));
  }

  submitSearch(form: NgForm): void {
    this.store.dispatch(new sharedActions.SetSearchTerm(form.value.searchTerm));
    this.search.emit(form.value);
  }
}
