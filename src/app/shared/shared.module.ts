import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { StoreModule } from '@ngrx/store';

import { SearchComponent } from './search/search.component';
import { reducer } from './state/shared.reducer';
import { DataModule } from '../data/data.module';

@NgModule({
  declarations: [SearchComponent],
  imports: [
    CommonModule,
    FormsModule,
    FontAwesomeModule,
    StoreModule.forFeature('shared', reducer)
  ],
  exports: [
    CommonModule,
    FormsModule,
    FontAwesomeModule,
    SearchComponent,
    DataModule
  ]
})
export class SharedModule { }
